/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package photoeditor;

import java.awt.image.BufferedImage;

/**
 *
 * @author 01
 */
public class Editor {
    
    BufferedImage makeGray(BufferedImage image){
        BufferedImage img = image;
        for (int i = 0; i < image.getHeight(); i++) {
            for (int j = 0; j < image.getWidth(); j++) {
                Pixel pixel = new Pixel(image, j, i);
                int avg = (pixel.red+pixel.green+pixel.blue)/3;
                pixel.setPixle((pixel.alfa <<24) | (avg<<16) | (avg<<8) | avg);
                img.setRGB(j, i, pixel.getPixle());
            }
        }
        return img;
    }
    BufferedImage makeRedless(BufferedImage image){
        BufferedImage img = image;
        for (int i = 0; i < image.getHeight(); i++) {
            for (int j = 0; j < image.getWidth(); j++) {
                Pixel pixel = new Pixel(image, j, i);
                pixel.setPixle((pixel.alfa <<24) | (0<<16) | (pixel.green<<8) | pixel.blue);
                img.setRGB(j, i, pixel.getPixle());
            }
        }
        return img;
    }
    BufferedImage makeGreenless(BufferedImage image){
        BufferedImage img = image;
        for (int i = 0; i < image.getHeight(); i++) {
            for (int j = 0; j < image.getWidth(); j++) {
                Pixel pixel = new Pixel(image, j, i);
                pixel.setPixle((pixel.alfa <<24) | (pixel.red<<16) | (0<<8) | pixel.blue);
                img.setRGB(j, i, pixel.getPixle());
            }
        }
        return img;
    }
    BufferedImage makeBlueless(BufferedImage image){
        BufferedImage img = image;
        for (int i = 0; i < image.getHeight(); i++) {
            for (int j = 0; j < image.getWidth(); j++) {
                Pixel pixel = new Pixel(image, j, i);
                pixel.setPixle((pixel.alfa <<24) | (pixel.red<<16) | (pixel.green<<8) | 0);
                img.setRGB(j, i, pixel.getPixle());
            }
        }
        return img;
    }
}
