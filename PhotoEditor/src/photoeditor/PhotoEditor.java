/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package photoeditor;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 *
 * @author 01
 */
public class PhotoEditor {
    public static void main(String[] args) throws IOException {
        BufferedImage image1 = null;
        File imageFile1 = null;
        String filePath = new UserInterface().getPath();
        FileManager fileManager = new FileManager();
        fileManager.setPath(filePath);
        Editor editor = new Editor();
        try {
            imageFile1 = new File(filePath);
            image1 = ImageIO.read(imageFile1);         
        } catch (Exception e) {
            System.out.println(e);
        }
        BufferedImage image2 = editor.makeGray(image1);
        File imageFile2 = null;
        try {
            imageFile2 = new File(fileManager.getNewPath());
            ImageIO.write(image2,fileManager.getFormat(), imageFile2);
        } catch (Exception e) {
            System.out.println(e);
        }      
    }
}
