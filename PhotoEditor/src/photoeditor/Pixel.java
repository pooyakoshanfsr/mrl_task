/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package photoeditor;

import java.awt.image.BufferedImage;

/**
 *
 * @author 01
 */
public class Pixel {
    int alfa;
    int red;
    int green;
    int blue;   
    private int pixle = (alfa<<24) | (red<<16) | (green<<8) | (blue);
    public void setPixle(int pixle) {
        this.pixle = pixle;
    }
    public int getPixle() {
        return pixle;
    }
    public Pixel(int alfa,int red,int green,int blue) {
        this.alfa = alfa;
        this.red = red;
        this.green = green;
        this.blue = blue;   
    }
    public Pixel(BufferedImage image,int x,int y) {
        int p = image.getRGB(x, y);
        this.alfa = (p>>24)&0xff;
        this.red = (p>>16)&0xff;
        this.green = (p>>8)&0xff;
        this.blue = p&0xff;   
    } 
}
