/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package photoeditor;

/**
 *
 * @author 01
 */
public class FileManager {
    private String path; 
    private String fileName;
    private String format;
    String[] pathArray;

    public String getFormat() {
        return format;
    }
    public void setPath(String path) {
        this.path = path;
        pathArray = path.split("\\\\");
        String temp2 = pathArray[pathArray.length - 1];
        String[] temp3 = temp2.split("\\.");
        fileName = temp3[0];
        format = temp3[1];
    }
    public String getPath() {
        return path;
    }
    public String getFileName() {
        return fileName;
    }
    public String getNewPath(){
        String newPath = "";
        for (int i = 0; i < pathArray.length-1 ; i++) {
            newPath+=pathArray[i] + "\\\\";
        }
        newPath+= fileName + " - Gray." + format; 
        return newPath;
    }
}
