/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hangman;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author 01
 */
public class CharecterChecker {   
    private static List<Character> wrongCheckedLetter = new ArrayList<Character>();
    private static List<Character> rightCheckedLetter = new ArrayList<Character>();

    public static List<Character> getRightCheckedLetter() {
        return rightCheckedLetter;
    }
    public static List<Character> getWrongCheckedLetter() {
        return wrongCheckedLetter;
    }
    static boolean check(char c,String choosed){
        char[] letters = choosed.toCharArray();
        int temp_Counter = 0;
        for (char d : letters) {
            temp_Counter++;
            if (d == c) {
                rightCheckedLetter.add(c);
                return true;
            }else if(d != c && temp_Counter == letters.length){
                wrongCheckedLetter.add(c);  
                return false;
            }
        }
         return false;
    }
}
