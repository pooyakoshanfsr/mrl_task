/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hangman;



/**
 *
 * @author 01
 */
public class GameController {
    static UserInterface userInterface = new UserInterface();
    static States states = States.READY;
    static boolean isWon = false;
    static boolean isLost = false;
    static int shots = 6;
    static int trys = 0;
    enum States {
        READY,
        LOST,
        WON,
        PLAY_TRY;
        void act() {
             switch (states) {
                     case READY:
                         getReady();
                         userInterface.sayReady();
                         break;
                     case LOST:
                         userInterface.sayLost();
                         loosing();
                         break;
                     case WON:
                         userInterface.sayWon();
                         winning();
                         break;
                     case PLAY_TRY:
                         while (!isWon && !isLost) {                             
                             userInterface.sayTry();
                             trying(userInterface.getChar());
                             userInterface.showWords(CharecterChecker.getRightCheckedLetter(), CharecterChecker.getWrongCheckedLetter());
                             userInterface.sayReamingShots(shots, trys);
                             if (trys == shots) {
                                 isLost = true;
                                 states = LOST;
                             }
                         }
                         if (states == LOST) {
                             GameController.States.LOST.act();
                         } else if (states == WON) {
                             GameController.States.WON.act();
                         }
                         break;
                     default:
                         throw new AssertionError("Unknown operations " + this);
                 }
             }
    }
    static private void getReady(){
        WordChooser.choose();
        System.out.println(WordChooser.choosed);
        states = States.PLAY_TRY;
    }
    static private void trying(char c){
       boolean b = CharecterChecker.check(c, WordChooser.choosed);
        if (b) {
            System.out.println(UserInterface.RIGHT_ANSWER); 
        }else if(!b){
            System.out.println(UserInterface.WRONG_ANSWER);
            trys++;
        }
    }
    static private void loosing(){
       //TODO
    }
    static private void winning(){
       //TODO
    }
    
}
